project(Palindrome)
cmake_minimum_required(VERSION 2.8)


if(EXISTS ${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)# Not CLion
else()
    include(${CMAKE_HOME_DIRECTORY}/conanbuildinfo.cmake) #Clion, with conanbuildinfo.cmake in root folder
endif()
conan_basic_setup()

include_directories(src)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

set(SOURCE_FILES
        src/Palindrome.cpp src/Palindrome.h
        )

set(PROGRAM_LIBRARY palindromelib)
add_library(${PROGRAM_LIBRARY} ${SOURCE_FILES})
target_link_libraries(${PROGRAM_LIBRARY} ${CONAN_LIBS})

#set(EXECUTABLE_NAME mytemplate)
#add_executable(${EXECUTABLE_NAME} src/main.cpp )
#target_link_libraries(${EXECUTABLE_NAME} ${PROGRAM_LIBRARY})

set(EXECUTABLE_NAME test_palindrome)
add_executable(${EXECUTABLE_NAME} src/test/${EXECUTABLE_NAME}.cpp )
target_link_libraries(${EXECUTABLE_NAME} ${PROGRAM_LIBRARY})


set(EXECUTABLE_NAME test_task1)
add_executable(${EXECUTABLE_NAME} src/test/${EXECUTABLE_NAME}.cpp )
target_link_libraries(${EXECUTABLE_NAME} ${PROGRAM_LIBRARY})

set(EXECUTABLE_NAME test_task2)
add_executable(${EXECUTABLE_NAME} src/test/${EXECUTABLE_NAME}.cpp )
target_link_libraries(${EXECUTABLE_NAME} ${PROGRAM_LIBRARY})

