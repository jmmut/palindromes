# Palindrome

Small exercise about palindromes. Head over src/test/ to see how to use the code. For instance:

```
using StringData = std::vector<std::string>;

TEST(GenericPalindromeIterator, analize) {
    auto small = StringData{"world", "yay"};
    for(const auto &palindrome : PalindromeIterator<StringData>{small}) {
        doAnalysis(palindrome);
        EXPECT_TRUE(isPalindrome(palindrome));
    }
}
```

## Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)

## Compiling and running

Linux:

```
git clone https://bitbucket.org/jmmut/palindrome.git
cd palindrome
mkdir build && cd build
conan install --build missing ..
cmake -G "Unix Makefiles" ..
make
./bin/test_palindrome
```

Windows (not tested):

```
git clone https://bitbucket.org/jmmut/palindrome.git
cd palindrome
conan install --build missing
cmake -G "Visual Studio 14 Win64"
cmake --build . --config Release
./bin/test_palindrome.exe
```
