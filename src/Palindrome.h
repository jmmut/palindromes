#ifndef PALINDROME_PALINDROME_H
#define PALINDROME_PALINDROME_H

#include <vector>
#include <stdexcept>
#include <sstream>

/**
 * A palindrome word is a symmetric list of values, such as {1}, {1, 1}, or {1, 2, 3, 2, 1}.
 *
 * {1, 2} or {1, 2, 3, 1, 1} are non-palindromes.
 */
template<typename Container>
inline bool isPalindrome(const Container &word) {
    for (size_t i = 0; i < word.size() / 2; ++i) {
        if (word[i] != word[word.size() -1 -i]) {
            return false;
        }
    }
    return true;
}

/**
 * template specialization for palindrome numbers such as "int n = 1331"
 *
 * This is a simple implementation. If performance is a concern, then manual conversion to a list of digits
 * (std::vector<int>) is probably faster, but should be measured.
 *
 * A manual conversion basically should extract units and divide by 10 iteratively, and then
 * call the general isPalindrome.
 */
template<>
inline bool isPalindrome(const int &number) {
    std::stringstream ss;
    ss << number;
    return isPalindrome(ss.str());
}

/**
 * This class allows traversal of a palindrome subset in a dataset, avoiding copying the data.
 *
 * See the tests for examples using this class in different scenarios.
 */
template<typename Data>
class PalindromeIterator {
public:
    using Word = typename Data::value_type;

    explicit PalindromeIterator(const Data &_data, size_t _index = 0) : data{_data}, index{_index} {
        while (index < data.size() && !isPalindrome(data[index])) {
            ++index;
        }
    }

    PalindromeIterator begin() {return PalindromeIterator{data, 0};}
    PalindromeIterator end() {return PalindromeIterator{data, data.size()};}

    const Word &operator*() {
        if (index != data.size()) {
            return data[index];
        } else {
            throw std::logic_error{"trying to access an after-the-end iterator"};
        }
    }

    bool operator!=(const PalindromeIterator &other) const {
        return index != other.index;
    }

    void operator++() {
        do {
            ++index;
        } while (index < data.size() && !isPalindrome(data[index]));
    }

private:
    const Data &data;
    size_t index;
};

#endif //PALINDROME_PALINDROME_H
