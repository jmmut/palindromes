
#include "Palindrome.h"
#include "gtest/gtest.h"
using Word = std::vector<int>;
using Data = std::vector<std::vector<int>>;

TEST(PalindromeIterator, count) {
    auto small = std::vector<std::vector<int>>{{1, 2, 3}, {1, 1}};
    auto palindromesCount = 0;
    for([[maybe_unused]] const auto &palindrome : PalindromeIterator<Data>{small}) {
        ++palindromesCount;
    }
    EXPECT_EQ(palindromesCount, 1);
}

inline void doAnalysis(const Word &word) {
    std::cout << "analysing " << (isPalindrome(word)? "palindrome" : "NON palindrome") << " word" << std::endl;
}

TEST(PalindromeIterator, analize) {
    auto small = std::vector<std::vector<int>>{{1, 2, 3}, {1, 1}};
    for(const auto &palindrome : PalindromeIterator<Data>{small}) {
        doAnalysis(palindrome);
        EXPECT_TRUE(isPalindrome(palindrome));
    }
}

TEST(PalindromeIterator, extract) {
    auto small = std::vector<std::vector<int>>{{1, 2, 3}, {1, 1}};
    Data palindromesData;
    for(const auto &palindrome : PalindromeIterator<Data>{small}) {
        palindromesData.push_back(palindrome);
    }
    EXPECT_EQ(palindromesData.size(), 1u);
    for (const auto &word : palindromesData) {
        EXPECT_TRUE(isPalindrome(word));
    }
}

TEST(PalindromeIterator, extractBigger) {
    auto data = std::vector<std::vector<int>>{
            {1, 2, 3},
            {1, 1},
            {1, 4, 4, 1},
            {1, 2, 4, 5, 1},
            {1, 6, 4, 6, 1},
            {1},
    };
    Data palindromesData;
    for(const auto &palindrome : PalindromeIterator<Data>{data}) {
        palindromesData.push_back(palindrome);
    }
    EXPECT_EQ(palindromesData.size(), 4u);
    for (const auto &word : palindromesData) {
        EXPECT_TRUE(isPalindrome(word));
    }
}
