
#include "Palindrome.h"
#include "gtest/gtest.h"

//////////// String tests

using StringData = std::vector<std::string>;

TEST(GenericPalindromeIterator, count) {
    auto small = std::vector<std::string>{"world", "yay"};
    auto palindromesCount = 0;
    for([[maybe_unused]] const auto &palindrome : PalindromeIterator<StringData>{small}) {
        ++palindromesCount;
    }
    EXPECT_EQ(palindromesCount, 1);
}

template<typename Container>
inline void doAnalysis(const Container &word) {
    std::cout << "analysing " << (isPalindrome(word)? "palindrome" : "NON palindrome") << " word" << std::endl;
}

TEST(GenericPalindromeIterator, analize) {
    auto small = std::vector<std::string>{"world", "yay"};
    for(const auto &palindrome : PalindromeIterator<StringData>{small}) {
        doAnalysis(palindrome);
        EXPECT_TRUE(isPalindrome(palindrome));
    }
}

TEST(GenericPalindromeIterator, extract) {
    auto small = std::vector<std::string>{"world", "yay"};
    decltype(small) palindromesData;
    for(const auto &palindrome : PalindromeIterator<StringData>{small}) {
        palindromesData.push_back(palindrome);
    }
    EXPECT_EQ(palindromesData.size(), 1u);
    for (const auto &word : palindromesData) {
        EXPECT_TRUE(isPalindrome(word));
    }
}

TEST(GenericPalindromeIterator, extractBigger) {
    auto data = std::vector<std::string>{
            "yay",
            "world",
            "level",
            "hello",
    };
    decltype(data) palindromesData;
    for(const auto &palindrome : PalindromeIterator<StringData>{data}) {
        palindromesData.push_back(palindrome);
    }
    EXPECT_EQ(palindromesData.size(), 2u);
    for (const auto &word : palindromesData) {
        EXPECT_TRUE(isPalindrome(word));
    }
}


//////////// Numeric tests

using NumberData = std::vector<int>;

TEST(NumericPalindromeIterator, count) {
    auto small = std::vector<int>{74347, 123};
    auto palindromesCount = 0;
    for([[maybe_unused]] const auto &palindrome : PalindromeIterator<NumberData>{small}) {
        ++palindromesCount;
    }
    EXPECT_EQ(palindromesCount, 1);
}

TEST(NumericPalindromeIterator, analize) {
    auto small = std::vector<int>{74347, 123};
    for(const auto &palindrome : PalindromeIterator<NumberData>{small}) {
        doAnalysis(palindrome);
        EXPECT_TRUE(isPalindrome(palindrome));
    }
}

TEST(NumericPalindromeIterator, extract) {
    auto small = std::vector<int>{74347, 123};
    decltype(small) palindromesData;
    for(const auto &palindrome : PalindromeIterator<NumberData>{small}) {
        palindromesData.push_back(palindrome);
    }
    EXPECT_EQ(palindromesData.size(), 1u);
    for (const auto &word : palindromesData) {
        EXPECT_TRUE(isPalindrome(word));
    }
}

TEST(NumericPalindromeIterator, extractBigger) {
    auto data = std::vector<int>{
            125125,
            4947,
            74347,
            11,
    };
    decltype(data) palindromesData;
    for(const auto &palindrome : PalindromeIterator<NumberData>{data}) {
        palindromesData.push_back(palindrome);
    }
    EXPECT_EQ(palindromesData.size(), 2u);
    for (const auto &word : palindromesData) {
        EXPECT_TRUE(isPalindrome(word));
    }
}
