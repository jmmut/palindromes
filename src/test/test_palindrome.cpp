
#include "Palindrome.h"
#include "gtest/gtest.h"

TEST(DependencyTest, gtest) {
    std::cout << "hello test" << std::endl;
}

TEST(Palindrome, even) {
    auto minimal = std::vector<int>{};
    EXPECT_TRUE(isPalindrome(minimal));

    auto small = std::vector<int>{1, 1};
    EXPECT_TRUE(isPalindrome(small));

    auto big = std::vector<int>{1, 6, 2, 8, 9, 9, 8, 2, 6, 1};
    EXPECT_TRUE(isPalindrome(big));
}

TEST(Palindrome, odd) {
    auto minimal = std::vector<int>{1};
    EXPECT_TRUE(isPalindrome(minimal));

    auto small = std::vector<int>{1, 2, 1};
    EXPECT_TRUE(isPalindrome(small));

    auto big = std::vector<int>{1, 6, 2, 8, 9, 5, 9, 8, 2, 6, 1};
    EXPECT_TRUE(isPalindrome(big));
}

TEST(NonPalindrome, even) {
    auto small = std::vector<int>{1, 3};
    EXPECT_FALSE(isPalindrome(small));

    auto big = std::vector<int>{1, 1, 2, 8, 9, 9, 8, 2, 6, 1};
    EXPECT_FALSE(isPalindrome(big));
}

TEST(NonPalindrome, odd) {

    auto small = std::vector<int>{1, 2, 2};
    EXPECT_FALSE(isPalindrome(small));

    auto big = std::vector<int>{1, 6, 2, 8, 9, 5, 9, 8, 2, 2, 1};
    EXPECT_FALSE(isPalindrome(big));
}

